package az.vitanur.task.service;

import az.vitanur.task.dto.UserResponse;
import az.vitanur.task.dto.security.LoginRequest;
import az.vitanur.task.dto.security.LoginResponse;
import az.vitanur.task.dto.security.RegisterDto;

public interface AuthenticationService {

   LoginResponse login(LoginRequest request);
   LoginResponse register(RegisterDto request);

   UserResponse getUserFullInfo(Long id);
}
