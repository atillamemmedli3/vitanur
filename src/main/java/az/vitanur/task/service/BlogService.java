package az.vitanur.task.service;

import az.vitanur.task.dto.BlogCommentDto;
import az.vitanur.task.dto.BlogRequest;
import az.vitanur.task.dto.BlogResponse;
import az.vitanur.task.entity.BlogComment;
import az.vitanur.task.dto.BlogSearchCriteria;

import java.util.List;

public interface BlogService {
    BlogResponse insert(BlogRequest request);

    void insertComment(Long id, BlogCommentDto comment);

    List<BlogResponse> filter(BlogSearchCriteria criteria);

    BlogResponse getBlogFullInfo(Long id);
}
