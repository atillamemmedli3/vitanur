package az.vitanur.task.service.impl;

import az.vitanur.task.dto.BlogCommentDto;
import az.vitanur.task.dto.BlogRequest;
import az.vitanur.task.dto.BlogResponse;
import az.vitanur.task.dto.BlogSearchCriteria;
import az.vitanur.task.entity.Blog;
import az.vitanur.task.entity.BlogComment;
import az.vitanur.task.entity.User;
import az.vitanur.task.repository.BlogCommentRepository;
import az.vitanur.task.repository.BlogFilterRepository;
import az.vitanur.task.repository.BlogRepository;
import az.vitanur.task.repository.UserRepository;
import az.vitanur.task.service.BlogService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BlogServiceImpl implements BlogService {

    private final BlogRepository blogRepository;
    private final BlogFilterRepository filterRepository;
    private final ModelMapper mapper;
    private final HttpServletRequest httpServletRequest;
    private final UserRepository userRepository;
    private final BlogCommentRepository blogCommentRepository;


    @Override
    public BlogResponse insert(BlogRequest request) {
        User user = getUserByUsername();
        Blog blog = blogDtoToEntity(request);
        blog.setCreatedBy(user);
        blog.setBlogComment(List.of(new BlogComment()));
        Blog saved = blogRepository.save(blog);
        return mapper.map(saved, BlogResponse.class);
    }

    @Override
    public void insertComment(Long id, BlogCommentDto commentDto) {
        BlogComment blogComment = blogCommentDtoToEntity(commentDto);
        Blog blog = fetchById(id);
        blog.getBlogComment().add(blogComment);
        blogCommentRepository.save(blogComment);
        blogRepository.save(blog);
    }


    @Override
    public List<BlogResponse> filter(BlogSearchCriteria criteria) {
        List<Blog> blogs = filterRepository.filter(criteria);
        return blogs.stream()
                .map((blog) -> mapper.map(blog, BlogResponse.class))
                .toList();
    }

    @Override
    public BlogResponse getBlogFullInfo(Long id) {
        Blog blog = fetchById(id);
        blog.setViewedCount(blog.getViewedCount() + 1);
        return blogEntityToDto(blog);
    }

    private Blog fetchById(Long id) {
        return blogRepository.findById(id).orElseThrow(
                () -> new RuntimeException("BLOG_NOT_FOUND"));
    }

    private BlogResponse blogEntityToDto(Blog blog) {
        return mapper.map(blog, BlogResponse.class);
    }

    private Blog blogDtoToEntity(BlogRequest request) {
        return mapper.map(request, Blog.class);
    }

    private BlogComment blogCommentDtoToEntity(BlogCommentDto commentDto) {
        return mapper.map(commentDto, BlogComment.class);
    }

    private User getUserByUsername() {
        String name = httpServletRequest.getUserPrincipal().getName();
        return userRepository.findByUsername(name).orElseThrow(() -> new RuntimeException("USER_NOT_FOUND"));
    }
}
