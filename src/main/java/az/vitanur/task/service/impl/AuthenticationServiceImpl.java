package az.vitanur.task.service.impl;

import az.vitanur.task.config.JwtService;
import az.vitanur.task.dto.UserResponse;
import az.vitanur.task.dto.security.LoginRequest;
import az.vitanur.task.dto.security.LoginResponse;
import az.vitanur.task.dto.security.RegisterDto;
import az.vitanur.task.entity.Permission;
import az.vitanur.task.entity.User;
import az.vitanur.task.repository.UserRepository;
import az.vitanur.task.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthenticationServiceImpl implements AuthenticationService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    private final ModelMapper modelMapper;

    @Override
    public LoginResponse register(RegisterDto request) {
        User user = createUserFromRegisterDto(request);
        userRepository.save(user);
        String jwtToken = jwtService.generateToken(user);
        return buildLoginResponse(jwtToken);
    }

    @Override
    public UserResponse getUserFullInfo(Long id) {
        User user = userIfExists(id);
        UserResponse map = modelMapper.map(user, UserResponse.class);
        return map;
    }


    @Override
    public LoginResponse login(LoginRequest request) {
        authenticateUser(request.getUsername(), request.getPassword());
        User user = findUserByUsername(request.getUsername());
        String jwtToken = jwtService.generateToken(user);
        return buildLoginResponse(jwtToken);
    }

    private User createUserFromRegisterDto(RegisterDto request) {
        return User.builder()
                .firstName(request.getFirstname())
                .lastName(request.getLastname())
                .email(request.getEmail())
                .username(request.getUsername())
                .password(passwordEncoder.encode(request.getPassword()))
                .phoneNumber(request.getPhoneNumber())
                .permission(List.of(Permission.builder().name(request.getRole()).build()))
                .isEnabled(true)
                .credentialsNonExpired(true)
                .isAccountNonExpired(true)
                .isAccountNonLocked(true)
                .build();
    }

    private void authenticateUser(String username, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    }

    private User findUserByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow();
    }

    private LoginResponse buildLoginResponse(String jwtToken) {
        return LoginResponse.builder().jwt(jwtToken).build();
    }

    private User userIfExists(Long id) {
        return userRepository.findById(id).orElseThrow(() ->
                new RuntimeException("USER_NOT_FOUND"));
    }

}
