package az.vitanur.task.repository;

import az.vitanur.task.dto.BlogSearchCriteria;
import az.vitanur.task.entity.Blog;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class BlogFilterRepository {
    private final EntityManager entityManager;

    public List<Blog> filter(BlogSearchCriteria criteria) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Blog> query = criteriaBuilder.createQuery(Blog.class);
        Root<Blog> root = query.from(Blog.class);

        Predicate predicate = criteriaBuilder.conjunction();

        if (!StringUtils.isEmpty(criteria.getName())) {
            predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("name"), criteria.getName()));
        }

        if (!StringUtils.isEmpty(criteria.getTitle())) {
            predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("title"), criteria.getTitle()));
        }

        if (criteria.getPublished() != null) {
            predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("published"), criteria.getPublished()));
        }

        if (criteria.getPublishedDate() != null) {
            predicate = criteriaBuilder.and(predicate, criteriaBuilder.equal(root.get("publishedDate"), criteria.getPublishedDate()));
        }

        query.where(predicate);

        List<Blog> resultList = entityManager.createQuery(query).getResultList();
        return resultList;
    }
}
