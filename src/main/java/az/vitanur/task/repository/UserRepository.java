package az.vitanur.task.repository;

import az.vitanur.task.entity.Blog;
import az.vitanur.task.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);

}
