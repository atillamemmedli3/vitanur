package az.vitanur.task.entity;


import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
public class Blog extends BaseEntity {
    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String content;

    @Column(nullable = false)
    private String cover;

    @Column(name = "viewed_count", nullable = false)
    private Long viewedCount = 0L;

    @Column(name = "published_date", nullable = false)
    @CreationTimestamp
    private LocalDateTime publishedDate;

    @Column(nullable = false)
    private Boolean published = true;

    @OneToMany(fetch = FetchType.EAGER)
    private List<BlogComment> blogComment = new ArrayList<>();

    @OneToMany(fetch = FetchType.EAGER , cascade = CascadeType.PERSIST)
    private List<BlogImage> blogImage;
}