package az.vitanur.task.entity;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "user")
@Builder
@AllArgsConstructor
public class User extends BaseEntity implements UserDetails{
    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    private String firstName;

    private String lastName;

    @Column(name = "phone_number")
    private String phoneNumber;

    @Column(nullable = false)
    private String email;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<UserType> userType;

    @ManyToMany(fetch = FetchType.EAGER ,cascade = CascadeType.PERSIST)
    private List<Permission> permission;


    boolean isEnabled;
    boolean credentialsNonExpired;
    boolean isAccountNonLocked;
    boolean isAccountNonExpired;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of();
    }
}

