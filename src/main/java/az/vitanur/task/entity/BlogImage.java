package az.vitanur.task.entity;

import jakarta.persistence.*;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "blog_image")
public class BlogImage extends BaseEntity {
    @Column(name = "file_path")
    private String filePath;

    @Column(name = "alt_text")
    private String altText;
}