package az.vitanur.task.entity;

import jakarta.persistence.*;
import lombok.*;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
@Table(name = "blog_comment")
public class BlogComment extends BaseEntity {

    private String comment;

}