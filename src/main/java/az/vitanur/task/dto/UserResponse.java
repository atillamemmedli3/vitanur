package az.vitanur.task.dto;

import az.vitanur.task.entity.Permission;
import az.vitanur.task.entity.User;
import az.vitanur.task.entity.UserType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserResponse {
    private Long id;
    private Date createdAt;
    private Date updatedAt;
    private User createdBy;
    private User updatedBy;
    private Boolean status;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private List<UserTypeDto> userType;
    private List<PermissionDto> permission;
}
