package az.vitanur.task.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogSearchCriteria {
    private String name;
    private String title;
    private Boolean published;
    private LocalDateTime publishedDate;
}
