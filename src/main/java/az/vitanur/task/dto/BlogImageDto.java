package az.vitanur.task.dto;

import lombok.Data;

@Data
public class BlogImageDto {
    private String filePath;
    private String altText;
}
