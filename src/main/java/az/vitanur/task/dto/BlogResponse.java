package az.vitanur.task.dto;

import az.vitanur.task.entity.BlogComment;
import az.vitanur.task.entity.BlogImage;
import az.vitanur.task.entity.User;
import lombok.*;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BlogResponse {
    private Long id;
    private String name;
    private String title;
    private String content;
    private String cover;
    private Long viewedCount;
    private LocalDateTime publishedDate;
    private Boolean published;

    private Long createdById;
    private String createdByUsername;

    private List<BlogCommentDto> blogComment;

    private List<BlogImageDto> blogImage;
}
