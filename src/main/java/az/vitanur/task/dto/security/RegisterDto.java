package az.vitanur.task.dto.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDto {
    private String username;
    private String firstname;
    private String lastname;
    private String phoneNumber;
    private String password;
    private String email;
    private String role;


}
