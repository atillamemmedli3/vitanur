package az.vitanur.task.controller;

import az.vitanur.task.dto.BlogCommentDto;
import az.vitanur.task.dto.BlogRequest;
import az.vitanur.task.dto.BlogResponse;
import az.vitanur.task.dto.BlogSearchCriteria;
import az.vitanur.task.service.BlogService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/blogs")
public class BlogController {
    private final BlogService blogService;

    @PostMapping("/insert")
    public BlogResponse insert(@RequestBody BlogRequest request) {
        return blogService.insert(request);
    }

    @PutMapping("/{id}/insertComment")
    public void insertComment(@PathVariable Long id,
                              @RequestBody BlogCommentDto comment) {
        blogService.insertComment(id, comment);
    }

    @GetMapping("/filter")
    public List<BlogResponse> filter(@RequestBody BlogSearchCriteria criteria) {
        return blogService.filter(criteria);
    }

    @GetMapping("/{id}/getBlogFullInfo")
    public BlogResponse getBlogFullInfo(@PathVariable Long id) {
        return blogService.getBlogFullInfo(id);
    }


}
