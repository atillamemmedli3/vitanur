package az.vitanur.task.controller;

import az.vitanur.task.dto.UserResponse;
import az.vitanur.task.dto.security.LoginRequest;
import az.vitanur.task.dto.security.LoginResponse;
import az.vitanur.task.dto.security.RegisterDto;
import az.vitanur.task.service.AuthenticationService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
@Slf4j
public class UserController {
    private final AuthenticationService authenticationService;

    @PostMapping("/sign-in")
    public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest loginDto) {
        log.info("Login request by user {}", loginDto.getUsername());
        return ResponseEntity.ok(authenticationService.login(loginDto));
    }

    @PostMapping("/registration")
    @ResponseStatus(HttpStatus.CREATED)
    public void register(@RequestBody RegisterDto request) {
        authenticationService.register(request);
    }

    @GetMapping("/{id}")
    public UserResponse getUserFullInfo(@PathVariable Long id) {
        return authenticationService.getUserFullInfo(id);
    }


}
